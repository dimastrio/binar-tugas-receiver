package id.dimas.mybroadcastreceiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.google.android.gms.auth.api.phone.SmsRetriever
import com.google.android.gms.common.api.CommonStatusCodes
import com.google.android.gms.common.api.Status

class SmsReceiver : BroadcastReceiver() {

    var smsBroadcastReceiverListener: SmsBroadcastReceiverListener? = null

    override fun onReceive(context: Context, intent: Intent) {

        if (SmsRetriever.SMS_RETRIEVED_ACTION == intent.action) {
            val extras = intent.extras
            val smsRetrieverStatus = extras?.get(SmsRetriever.EXTRA_STATUS) as Status

            when (smsRetrieverStatus.statusCode) {

                CommonStatusCodes.SUCCESS -> {

                    val messageIntent =
                        extras.getParcelable<Intent>(SmsRetriever.EXTRA_CONSENT_INTENT)
                    smsBroadcastReceiverListener?.onSucces(messageIntent)
                }

                CommonStatusCodes.TIMEOUT -> {
                    smsBroadcastReceiverListener?.onFailure()
                }

            }

        }

//        if (extras != null){
//            val sms = extras.get("pdus") as Array<*>
//            for (i in sms.indices){
//                val format = extras.getString("format")
//
//                val smsMessage =
//                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                        SmsMessage.createFromPdu(sms[i] as ByteArray, format)
//                    } else {
//                        SmsMessage.createFromPdu(sms[i] as ByteArray)
//                    }
//
//
//                val phoneNumber = smsMessage.originatingAddress
//                val messageText = smsMessage.messageBody.toString()
//
//                textView_otp?.text = messageText
//
//            }
    }

    interface SmsBroadcastReceiverListener {
        fun onSucces(intent: Intent?)
        fun onFailure()
    }
}

